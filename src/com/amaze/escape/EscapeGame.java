package com.amaze.escape;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

public class EscapeGame {

	public static void main(String[] args) {
		setUpDisplay();
		setUpVBOs();
		setUpCamera();
		setUpShaders();
		setUpStates();
		setUpTimer();
		while (!Display.isCloseRequested()) {
			render();
			checkInput();
			Display.update();
			Display.sync(60);
		}
	}

	private static void setUpDisplay() {
		try {
			Display.setDisplayMode(new DisplayMode(800, 600));
			Display.setVSyncEnabled(true);
			Display.setTitle("Memory & Escape");
			Display.create();
		} catch (LWJGLException e) {
			System.err.println("Display create failed");
			Display.destroy();
			System.exit(1);
		}
		
	}

	private static void setUpVBOs() {
		// TODO Auto-generated method stub
		
	}

	private static void setUpCamera() {
		// TODO Auto-generated method stub
		
	}

	private static void setUpShaders() {
		// TODO Auto-generated method stub
		
	}

	private static void setUpStates() {
		// TODO Auto-generated method stub
		
	}

	private static void setUpTimer() {
		// TODO Auto-generated method stub
		
	}

	private static void render() {
		// TODO Auto-generated method stub
		
	}

	private static void checkInput() {
		// TODO Auto-generated method stub
		
	}
	
}
